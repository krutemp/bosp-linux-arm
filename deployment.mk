
.PHONY: deployment

all: deployment
deployment: build

include deployment/$(PLATFORM)/deployment.mk
