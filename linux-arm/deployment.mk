
################################################################################
# Installation package BUILDING
################################################################################
ifdef CONFIG_DEPLOYMENT_BUILD_UPDATE_ZIP

.PHONY: build-update-zip

# Source and destination folders
export SRC=$(BUILD_DIR)
export DST=$(BASE_DIR)/deployment/$(PLATFORM)/update_zip

# Self installer dir and deployer
export SELFINSTALLER=$(BASE_DIR)/deployment/linux-common/selfinstaller
#export DEPLOYER=$(BASE_DIR)/deployment/$(PLATFORM)/deployer

# Installation packages
export INSTALLSH=$(BASE_DIR)/deployment/$(PLATFORM)/install-arm.sh
export PKG=$(BASE_DIR)/deployment/$(PLATFORM)/update.tar.bz2
export SELFPKG=$(BASE_DIR)/deployment/$(PLATFORM)/update.sh

# Setup for deployment to pandaboard
export PANDABOARD_IP=$(CONFIG_DEPLOYMENT_LINUX_ARM_PANDABOARD_IP)
export PANDABOARD_USER=$(CONFIG_DEPLOYMENT_LINUX_ARM_PANDABOARD_USER)
export SSH_KEY=$(BASE_DIR)/deployment/$(PLATFORM)/$(CONFIG_DEPLOYMENT_LINUX_ARM_SSH_KEY)

# Paths and tools
export DEVDIR=$(CONFIG_BOSP_RUNTIME_PATH)
export RWPATH=$(CONFIG_BOSP_RUNTIME_RWPATH)
export STRIP=$(shell echo $(CXX) | sed '{s/g++\(-[0-9\.]\+\)\?$$/strip/}')

.PHONY: deployment
deployment: build-update-zip
#build-update-zip: build
build-update-zip:
	@#echo STRIP $(STRIP)
	@echo "Remove previous build packages [$$PKG]..."
	@rm -rf $$PKG &>/dev/null
	@echo "Remove previous build packages [$$SELFPKG]..."
	@rm -rf $$SELFPKG &>/dev/null
	@deployment/linux-common/extract_image
	@deployment/linux-common/gen_update
	@echo
	@echo "BBQ installation package [$$PKG] ready for installation on Linux-arm devices"
	@echo "Either copy if on the target device, or install to a locally attached device, by:"
	@echo " $$ make apply-update-zip"
	@echo

.PHONY: apply-update-zip

################################################################################
# Installation package DEPLOYMENT
################################################################################
ifdef CONFIG_DEPLOYMENT_APPLY_UPDATE_ZIP

deployment: apply-update-zip
apply-update-zip: build-update-zip 
	@echo "Deploying [$$PKG] to device..."
	@deployment/linux-common/apply_update $(SELFPKG) $(PANDABOARD_IP) $(PANDABOARD_USER) $(SSH_KEY)
	@echo
	@echo "Installation COMPLETED!"
	@echo
	@echo "The BarbequeRTRM is now ready to be used, enjoy the grill!"

else # CONFIG_DEPLOYMENT_APPLY_UPDATE_ZIP

apply-update-zip:
	$(warning update.zip deployment disabled by BOSP configuration")
	$(error deployment failed)

endif # CONFIG_DEPLOYMENT_APPLY_UPDATE_ZIP

endif # CONFIG_DEPLOYMENT_BUILD_UPDATE_ZIP
