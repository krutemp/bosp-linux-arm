#!/bin/bash

BOSPDIR=/opt/bosp

echo "Running BOSP Installer on $HOSTNAME"
#Check if dir exist
if [ ! -d $BOSPDIR ]; then
	echo "*** ERROR *** The user $BOSPDIR doesn't exist";
	echo "Please create $BOSPDIR and give $USER ownership and write access. Then run the installer again.";
	echo "Something like:";
	echo "sudo mkdir -p $BOSPDIR";
	echo "sudo chown $USER.$USER $BOSPDIR";
	exit -1;	
fi
#Check if the user has write permit on the dir
if [ ! -w $BOSPDIR ]; then
	echo "*** ERROR *** The user $USER doesn't have write permission for $BOSPDIR";
	echo "Please give $USER write access to $BOSPDIR. Then run the installer again.";
	echo "Something like:";
	echo "sudo chmod -R u+w $BOSPDIR";
	exit -1;		
fi

#After here the installer should be able to delete everything in the dir then install BOSP

echo "Cleaning up $BOSPDIR";
rm -rf $BOSPDIR/*

if [ -d /home/bosp/Desktop ]; then
	echo "Cleaning up links in /home/bosp/Desktop";
	find /home/bosp/Desktop/ \( -name *.desktop -or -name *.demo \) \
		|	\
	(while read FILE; do
		rm $FILE
	done)
fi

# Copies everything to $BOSPDIR
echo "Moving files to $BOSPDIR";
cp -a ./* $BOSPDIR/

echo "Copying shortcuts to desktop";
find $BOSPDIR/data/ -name *.desktop \
	|	\
(while read FILE; do
	cp $FILE /home/bosp/Desktop/
done)

find $BOSPDIR/bin/ -name *.demo \
	|	\
(while read FILE; do
	filename=$(basename "$FILE")
	ln -s $FILE /home/bosp/Desktop/$filename
done)

rm $BOSPDIR/install.sh
echo "BOSP installed correctly in $BOSPDIR";

