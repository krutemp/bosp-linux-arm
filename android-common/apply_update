#!/bin/bash

# Get current BOSP configuration
. build/configuration/bosp-config

# Setup some locals
BOSP_BASE="`pwd`"
ADB=${CONFIG_DEPLOYMENT_ANDROID_SDK_PATH}/platform-tools/adb
UPDATER_HOST="${BOSP_BASE}/deployment/android-common/updater.arm"
PACKAGE_HOST="${BOSP_BASE}/deployment/${CONFIG_TARGET_PLATFORM}/update.zip"
UPDATER_TRGT="${CONFIG_DEPLOYMENT_TMP_ON_DEVICE}/updater"
PACKAGE_TRGT="${CONFIG_DEPLOYMENT_TMP_ON_DEVICE}/update.zip"

function trim_numbers() {
	echo $1 |tr -dc 0-9;
}

# Set the target device, if required
if [ ${CONFIG_DEPLOYMENT_APPLY_UPDATE_ZIP_DEVICE} != "default" ]; then
	export ANDROID_SERIAL=${CONFIG_DEPLOYMENT_APPLY_UPDATE_ZIP_DEVICE}
	echo "Android target device set to [$ANDROID_SERIAL]"
fi

# Check for "updater" script already available in the target
echo "Checking for 'updater' tool on Android device..."
UPDATER_PRESENT=$($ADB shell updater | grep "not found" | wc -l)
if [ $UPDATER_PRESENT -eq 1 ]; then
	echo "Updater NOT present on the Android [$ANDROID_SERIAL] device"
	if [ "$CONFIG_DEPLOYMENT_DEPLOY_UPDATER" == "y" ]; then
		echo "Pushing [$UPDATER_HOST] into [$ANDROID_SERIAL:$UPDATER_TRGT]..."
		$ADB push $UPDATER_HOST $UPDATER_TRGT
		$ADB shell "chmod 755 $UPDATER_TRGT"
		REMOVE_UPDATER=1
	fi
else
	UPDATER_TRGT="updater"
fi

# Deploying installation package
echo "Deploying package..."
$ADB push $PACKAGE_HOST $PACKAGE_TRGT

# Install package (if required)
echo "Installing package..."
$ADB shell "$UPDATER_TRGT 3 1 $PACKAGE_TRGT"

echo "Cleanup installer..."
$ADB shell "rm  $PACKAGE_TRGT"
if [ "$REMOVE_UPDATER" == "1" ]; then
	$ADB shell "rm $UPDATER_TRGT"
fi
