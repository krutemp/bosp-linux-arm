#!/bin/bash
echo ""
echo "Self Extracting Installer"
echo ""

export TMPDIR=`mktemp -d /tmp/selfextract.XXXXXX`

ARCHIVE=`awk '/^__ARCHIVE_BELOW__/ {print NR + 1; exit 0; }' $0`

tail -n+$ARCHIVE $0 | tar xj -C $TMPDIR

#Goes to the temp dir (/tmp/selfextract.XXXXXX) and runs the installer
CDIR=`pwd`
cd $TMPDIR
./install.sh

cd $CDIR
rm -rf $TMPDIR

exit 0

#Note: we need an empty line after this
__ARCHIVE_BELOW__
