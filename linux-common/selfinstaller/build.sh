#!/bin/bash

# Compresses the files in $DST into $PKG (updater.tar.bz2)
cd $DST
#cp $SELFINSTALLER/install.sh .
cp $INSTALLSH ./install.sh
tar cjf $PKG .
echo "[build.sh] File compression ok";

cd ..

if [ -e $PKG ]; then
	# Builds the self extracting file joining decompress.sh + $PKG in the self extracting file $SELFPKG
	cat $SELFINSTALLER/decompress.sh $PKG > $SELFPKG
	chmod +x $SELFPKG
else
    echo "[build.sh] $PKG does not exist"
    exit 1
fi

echo "[build.sh] $SELFPKG created"
exit 0
