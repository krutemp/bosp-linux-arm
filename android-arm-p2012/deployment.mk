
################################################################################
# Installation package BUILDING
################################################################################
ifdef CONFIG_DEPLOYMENT_BUILD_UPDATE_ZIP

.PHONY: build-update-zip

# Source and destination folders
export SRC=$(BUILD_DIR)
export DST=$(BASE_DIR)/deployment/$(PLATFORM)/update_zip

# Installation packages
export PKG=$(BASE_DIR)/deployment/$(PLATFORM)/update.zip

# Paths and tools
export DEVDIR=$(CONFIG_BOSP_RUNTIME_PATH)
export RWPATH=$(CONFIG_BOSP_RUNTIME_RWPATH)
export STRIP=$(shell echo $(CXX) | sed '{s/g++\(-[0-9\.]\+\)\?$$/strip/}')

deployment: build-update-zip
build-update-zip: build
	@#echo STRIP $(STRIP)
	@echo "Remove previous build packages [$$PKG]..."
	@rm -rf $$PKG &>/dev/null
	@deployment/android-common/extract_image
	@deployment/android-common/gen_update
	@echo
	@echo "BBQ installation package [$$PKG] ready for installation on Android devices"
	@echo "Either copy if on the target device, or install to a locally attached device, by:"
	@echo " $$ make apply-update-zip"
	@echo

.PHONY: apply-update-zip

################################################################################
# Installation package DEPLOYMENT
################################################################################
ifdef CONFIG_DEPLOYMENT_APPLY_UPDATE_ZIP

ADB := $(shell echo $(CONFIG_DEPLOYMENT_ANDROID_SDK_PATH)/platform-tools/adb | tr -d '"')

deployment: apply-update-zip
apply-update-zip: build-update-zip $(ADB)
	@echo "Deploying [$$PKG] to device..."
	@deployment/android-common/apply_update
	@echo
	@echo "Installation COMPLETED!"
	@echo
	@echo "The BarbequeRTRM is now ready to be used, enjoy the grill!"

else # CONFIG_DEPLOYMENT_APPLY_UPDATE_ZIP

apply-update-zip:
	$(warning update.zip deployment disabled by BOSP configuration")
	$(error deployment failed)

endif # CONFIG_DEPLOYMENT_APPLY_UPDATE_ZIP

endif # CONFIG_DEPLOYMENT_BUILD_UPDATE_ZIP
